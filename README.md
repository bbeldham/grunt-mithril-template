Using this repo as a project template (change NAME below, run in shell):


```
#!bash

projectname=NAME; git archive --format=tar.gz --prefix=$projectname/ --remote=ssh://git@bitbucket.org/sibsibsib/grunt-mithril-template.git master | tar -zx
```

* `cd NAME; npm install`
* run `grunt dev` to serve on localhost:8000
* run `grunt build` to build