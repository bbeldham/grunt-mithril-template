module.exports = {
    options: {
        compress: {},
        sourceMap: true,
    },
    js: {
        files: {
            './dist/app.min.js': ['./dist/app.js'],
            './dist/lib.min.js': ['./dist/lib.js'],
        }
    }
}
