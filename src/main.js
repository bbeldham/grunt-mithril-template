/** @jsx m */
var m = require('mithril');



var RootComponent = {
    controller: function() {
        return {
            model: m.prop()
        }
    },
    view: function(ctrl) {
        let name = ctrl.model();
        return <div>
            <div className="frm-field">
                <label>what is your name?</label>
                <input type="text" value={name || ''} oninput={m.withAttr('value', ctrl.model)}/>
            </div>
            <div>
                { !!name  ?  `Hello ${name} :)` : undefined}
            </div>
        </div>
    }
};


function init(config) {
    m.route(config.root_el, '/', {
        '/': RootComponent,
    });
}


module.exports = {
    init: init
};
